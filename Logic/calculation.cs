﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Musics;
using LogicMusic;


namespace LogicMusic
{
    public class Calculation
    {
        public static int GetLength(Disk musicdisk)
        {
            int totallength = 0;
            List<AbstractMusic> musics = musicdisk.Music;

            foreach (AbstractMusic music in musics)
            {
                totallength += music.Length;
            }

            return totallength;
        }
    }
}
