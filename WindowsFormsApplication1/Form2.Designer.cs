﻿namespace WindowsFormsApplication1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TotalLength = new System.Windows.Forms.Label();
            this.MusicListBox = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // TotalLength
            // 
            this.TotalLength.AutoSize = true;
            this.TotalLength.Location = new System.Drawing.Point(10, 152);
            this.TotalLength.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TotalLength.Name = "TotalLength";
            this.TotalLength.Size = new System.Drawing.Size(46, 17);
            this.TotalLength.TabIndex = 4;
            this.TotalLength.Text = "label1";
            // 
            // MusicListBox
            // 
            this.MusicListBox.FormattingEnabled = true;
            this.MusicListBox.ItemHeight = 16;
            this.MusicListBox.Location = new System.Drawing.Point(13, 13);
            this.MusicListBox.Margin = new System.Windows.Forms.Padding(4);
            this.MusicListBox.Name = "MusicListBox";
            this.MusicListBox.Size = new System.Drawing.Size(206, 132);
            this.MusicListBox.TabIndex = 5;
            this.MusicListBox.SelectedIndexChanged += new System.EventHandler(this.MusicListBox_SelectedIndexChanged);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 178);
            this.Controls.Add(this.MusicListBox);
            this.Controls.Add(this.TotalLength);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form2";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TotalLength;
        private System.Windows.Forms.ListBox MusicListBox;
    }
}

