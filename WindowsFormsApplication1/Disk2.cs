﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Musics;
using LogicMusic;

namespace LogicMusic
{
    public class Process
    {
        static void Disk(string[] args)
        {
            Disk disk = new Disk();
        }

        public static Disk Sborka()
        {
            Rap musicone = new Rap();
            musicone.Jenre = "City";
            musicone.Tematic = "Love";
            musicone.Length = 217;
            musicone.Author = "Noize MC";

            Rock musictwo = new Rock();
            musictwo.Jenre = "Hard";
            musictwo.Tematic = "Horror";
            musictwo.Length = 251;
            musictwo.Author = "Breaking Benjamin";

            Disk musicdisk = new Disk();
            Disk.AddMusic(musicone);
            Disk.AddMusic(musictwo);

            return musicdisk;
        }
    }
}

