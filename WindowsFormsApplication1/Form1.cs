﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Musics;
using LogicMusic;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            RefreshMusic();
        }

      

        private void RefreshMusic()
        {
            TotalLength.Text = Calculation.GetLength(Startoviy.musicdisk).ToString();
            MusicListBox.DataSource = null;
            MusicListBox.DataSource = Startoviy.music;
            MusicListBox.DisplayMember = "Name";
        }

  
    }
}
