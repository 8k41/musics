﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Musics;
using LogicMusic;

namespace Musics
{
   public class Startoviy
    {
        private static Disk _musicdisk = Process.Sborka();
        private static List<AbstractMusic> _music = musicdisk.Music;

        public static Disk musicdisk
        {
            get { return _musicdisk; }
            set { _musicdisk = value; }
        }
        public static  List<AbstractMusic> music
        {
            get { return _music; }
            set { _music = value; }
        }
    }
}
