﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Musics;
using LogicMusic;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            RefreshMusic();
        }

        private void RefreshMusic()
        {
            TotalLength.Text = Calculation.GetLength(Startoviy.musicdisk).ToString();
            MusicListBox.DataSource = null;
            MusicListBox.DataSource = Startoviy.music;
            MusicListBox.DisplayMember = "Name";
        }

        private void MusicListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

  
    }
}
