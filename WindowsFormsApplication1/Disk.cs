﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Musics;
using LogicMusic;

namespace Musics
{
    public class Disk
    {
        public static List<AbstractMusic> musics;

        public Disk()
        {
            if (musics == null)
            {
                musics = new List<AbstractMusic>();
            }
        }

        public static void AddMusic(AbstractMusic music)
        {
            musics.Add(music);
        }

        public static void DelMusic(Disk musicdisk, int index)
        {
            musics.RemoveAt(index);
        }

        public List<AbstractMusic> Music
        {
            get { return musics; }
        }
    }
}
