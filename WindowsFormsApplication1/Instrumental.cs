﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Musics;

namespace Musics
{
   public class Instrumental:AbstractMusic
    {
        private string _orchestre;

        public string Orchestre
        {
            get { return _orchestre; }
            set { _orchestre = value; }
        }
    }
}
